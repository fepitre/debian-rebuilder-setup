```shell
$ ansible-galaxy install -r requirements.yml
$ ansible-playbook playbook.yml
```

## accumulator.yml

This file contains credentials that are needed to push the build artifacts. If
you want to redeploy the current setup, copy the ansible-vault password you've
been given to `./vault_password_file`.

If you want to deploy your own setup you can create your own accumulator.yml
like this:

    cat > host_vars/default/accumulator.yml <<EOF
    accumulator_password: somevalue
    EOF

Note that this password needs to match the hash in
`roles/visualizers/files/htpasswd`.
